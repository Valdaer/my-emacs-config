(require 'elpy)

(elpy-enable)
(setq elpy-rpc-backend "jedi")
(setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
