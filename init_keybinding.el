(defun toggle-comment ()
  (interactive)
  (let ((start (line-beginning-position))
	(end (line-end-position)))
    (when (region-active-p)
      (setq start (save-excursion
		    (goto-char (region-beginning))
		    (beginning-of-line)
		    (point))
	    end (save-excursion
		  (goto-char (region-end))
		  (end-of-line)
		  (point))))
    (comment-or-uncomment-region start end)
    (setq deactivate-mark nil)))

(defun my-edbi-evaluate ()
  (interactive)
  (lexical-let ((buf (current-buffer)))
    (edbi:seq
     (edbi:dbview-query-editor-execute-command)
     (lambda ()
       (select-window (get-buffer-window buf))))))

(defun my-edbi-sql-mode-keys ()
  "Modify keymap used by edbi:sql-mode"
  (local-set-key (kbd "M-r") 'my-edbi-evaluate)
  (local-set-key (kbd "M-z") 'edbi:dbview-query-editor-history-back-command)
  (local-set-key (kbd "M-Z") 'edbi:dbview-query-editor-history-forward-command))

(defun my-org-mode-keys ()
  "Modify keymaps used by org-mode."
  (local-set-key (kbd "M-c") 'org-todo)
  (local-set-key (kbd "M-<prior>") 'org-priority-up)
  (local-set-key (kbd "M-<next>") 'org-priority-down)
  (local-set-key (kbd "M-<home>") 'org-do-promote)
  (local-set-key (kbd "M-<end>") 'org-do-demote))

(defun my-json-mode-keys ()
  "Modify keymaps used by json-mode"
  (local-set-key (kbd "C-r") 'json-mode-beautify))

;; needed to override ergoemacs change because of not working local setting org-todo in org-mode
(global-set-key (kbd "M-c") 'rename-file)

;; override Ctrl+/ to comment, removal from undo-tree-map keymap was neccesary for some ergoemacs versions
(global-set-key (kbd "C-/") 'toggle-comment)

(global-set-key (kbd "s-a") 'org-todo-list) ;; Open agenda TODO task list

;; tabbar navigation shortcuts
(global-set-key (kbd "C-<next>") 'awesome-tab-forward-tab)
(global-set-key (kbd "C-<prior>") 'awesome-tab-backward-tab)
(global-set-key (kbd "C-M-<next>") 'awesome-tab-forward-group)
(global-set-key (kbd "C-M-<prior>") 'awesome-tab-backward-group)
(global-set-key (kbd "C-S-<next>") 'awesome-tab-move-current-tab-to-right)
(global-set-key (kbd "C-S-<prior>") 'awesome-tab-move-current-tab-to-left)
(global-set-key (kbd "C-S-e") 'awesome-tab-counsel-switch-group)

(global-set-key (kbd "C-SPC") 'company-other-backend)
(global-set-key (kbd "C-M-SPC") 'company-yasnippet)

(global-set-key (kbd "M-x") 'smart-jump-go)
(global-set-key (kbd "M-X") 'smart-jump-back)
(global-set-key (kbd "M-z") 'smart-jump-references)

(global-set-key (kbd "C-M-c") 'rectangle-mark-mode)

(global-set-key (kbd "M-v") 'er/expand-region)

;; change to function with find in projectile/git based on context
(global-set-key (kbd "M-f") 'counsel-find-file)

(define-key back-button-mode-map (kbd "M-<left>") 'back-button-global-backward)
(define-key back-button-mode-map (kbd "M-<right>") 'back-button-global-forward)
(define-key back-button-mode-map (kbd "C-M-<left>") 'back-button-local-backward)
(define-key back-button-mode-map (kbd "C-M-<right>") 'back-button-local-forward)

(define-key ivy-minibuffer-map (kbd "C-S-f") #'ivy-previous-line-or-history)
(define-key ivy-minibuffer-map (kbd "C-r") #'swiper-query-replace)
(define-key ivy-minibuffer-map (kbd "C-S-r") #'swiper-all-query-replace)
(define-key ivy-minibuffer-map (kbd "M-x") #'ivy-dispatching-done)



;; Hooks
(add-hook 'org-mode-hook 'my-org-mode-keys)

(add-hook 'json-mode-hook 'my-json-mode-keys)

;; (add-hook 'edbi:sql-mode-hook 'my-edbi-sql-mode-keys)
