(require 'ivy)
(require 'counsel)

(setq enable-recursive-minibuffers t)

(setq ivy-use-virtual-buffers t)
(setq ivy-count-format "%d/%d ")
(ivy-mode 1)

(counsel-mode 1)

(require 'ivy-posframe)
(setq ivy-posframe-height-alist
      '((swiper . 15)
        (t      . 10)))

(setq ivy-posframe-display-functions-alist
      '((swiper          . nil)
        (swiper-isearch  . nil)
        (swiper-all  . nil)
        (swiper-query-replace  . nil)
        (swiper-all-query-replace  . nil)
        (complete-symbol . ivy-posframe-display-at-point)
        (t               . ivy-posframe-display-at-frame-center)))

(ivy-posframe-mode 1)

(require 'prescient)
(require 'ivy-prescient)

(setq prescient-filter-method '(literal regexp initialism fuzzy))
(setq ivy-prescient-retain-classic-highlighting t)
(setq ivy-prescient-sort-commands
      '(:not swiper swiper-isearch swiper-all ivy-switch-buffer counsel-switch-buffer
             counsel-buffer-or-recentf counsel-recentf counsel-git-log counsel-git-grep
             counsel-projectile-grep counsel-projectile-git-grep counsel-projectile-ag
             counsel-projectile-rg counsel-grep counsel-ag counsel-rg counsel-pt counsel-locate
             counsel-imenu counsel-yank-pop))

(setq ivy-re-builders-alist
      '((swiper . ivy--regex-plus)
        (swiper-isearch . ivy--regex-plus)
        (swiper-all . ivy--regex-plus)
        (counsel-git-log . ivy--regex-plus)
        (counsel-git-grep . ivy--regex-plus)
        (counsel-projectile-grep. ivy--regex-plus)
        (counsel-projectile-git-grep . ivy--regex-plus)
        (counsel-projectile-ag . ivy--regex-plus)
        (counsel-projectile-rg . ivy--regex-plus)
        (counsel-grep-or-swiper . ivy--regex-plus)
        (counsel-grep . ivy--regex-plus)
        (counsel-ag . ivy--regex-plus)
        (counsel-rg . ivy--regex-plus)
        (counsel-pt . ivy--regex-plus)
        (counsel-locate . ivy--regex-plus)
        (counsel-imenu . ivy--regex-plus)
        (counsel-yank-pop . ivy--regex-plus)
        (counsel-unicode-char . ivy--regex-plus)
        (t . ivy-prescient-re-builder)))

(prescient-persist-mode 1)
(ivy-prescient-mode 1)

(require 'all-the-icons-ivy)
(all-the-icons-ivy-setup)
