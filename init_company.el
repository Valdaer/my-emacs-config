(require 'company)

;; company delay until suggestions are shown
(setq company-idle-delay 0.2)

(setq company-minimum-prefix-length 2)

(setq company-backends
      '(company-capf
        company-files
        (company-dabbrev-code company-gtags company-etags company-keywords)
        company-dabbrev))

(add-hook 'nxml-mode-hook
          (lambda ()
            (make-local-variable 'company-backends)
            (setq company-backends (cons 'company-nxml (copy-tree company-backends)))))

(add-hook 'css-mode-hook
          (lambda ()
            (make-local-variable 'company-backends)
            (setq company-backends (cons 'company-css (copy-tree company-backends)))))

(require 'company-ansible)
(add-hook 'poly-ansible-mode-hook
          (lambda ()
            (make-local-variable 'company-backends)
            (setq company-backends (cons 'company-ansible (copy-tree company-backends)))))

;; (require 'company-edbi)
(add-hook 'sql-mode-hook
          (lambda ()
            (make-local-variable 'company-backends)
            (setq company-backends (cons 'company-edbi (copy-tree company-backends)))))

(require 'company-prescient)
(company-prescient-mode 1)

(require 'company-box)

(setq company-box-icons-all-the-icons
      `((Unknown . ,(all-the-icons-faicon "cog" :height 0.85 :v-adjust -0.02))
        (Text . ,(all-the-icons-octicon "file-text" :height 0.85))
        (Method . ,(all-the-icons-faicon "cube" :height 0.85 :v-adjust -0.02))
        (Function . ,(all-the-icons-faicon "cube" :height 0.85 :v-adjust -0.02))
        (Constructor . ,(all-the-icons-faicon "cube" :height 0.85 :v-adjust -0.02))
        (Field . ,(all-the-icons-material "loyalty" :height 0.85 :v-adjust -0.2))
        (Variable . ,(all-the-icons-material "loyalty" :height 0.85 :v-adjust -0.2))
        (Class . ,(all-the-icons-faicon "cogs" :height 0.85 :v-adjust -0.02))
        (Interface . ,(all-the-icons-material "control_point_duplicate" :height 0.85 :v-adjust -0.02))
        (Module . ,(all-the-icons-alltheicon "less" :height 0.85 :v-adjust -0.05))
        (Property . ,(all-the-icons-faicon "wrench" :height 0.85))
        (Unit . ,(all-the-icons-material "streetview" :height 0.85))
        (Value . ,(all-the-icons-faicon "tag" :height 0.85 :v-adjust -0.2))
        (Enum . ,(all-the-icons-material "library_books" :height 0.85))
        (Keyword . ,(all-the-icons-material "functions" :height 0.85))
        (Snippet . ,(all-the-icons-material "content_paste" :height 0.85))
        (Color . ,(all-the-icons-material "palette" :height 0.85))
        (File . ,(all-the-icons-faicon "file" :height 0.85))
        (Reference . ,(all-the-icons-faicon "cog" :height 0.85 :v-adjust -0.02))
        (Folder . ,(all-the-icons-faicon "folder" :height 0.85))
        (EnumMember . ,(all-the-icons-material "collections_bookmark" :height 0.85))
        (Constant . ,(all-the-icons-material "class" :height 0.85))
        (Struct . ,(all-the-icons-faicon "cogs" :height 0.85 :v-adjust -0.02))
        (Event . ,(all-the-icons-faicon "bolt" :height 0.85))
        (Operator . ,(all-the-icons-material "streetview" :height 0.85))
        (TypeParameter . ,(all-the-icons-faicon "cogs" :height 0.85 :v-adjust -0.02))
        (Template . ,(all-the-icons-material "settings_ethernet" :height 0.9)))
      company-box-icons-alist 'company-box-icons-all-the-icons)


(add-hook 'company-mode-hook 'company-box-mode)

(add-hook 'after-init-hook 'global-company-mode)

