;; To have consistent fonts accross all kinds of emacs(gui, dameon, console) aside from this setting there must be "Emacs.font: FONT" with exactly the same FONT as defined here.
(set-default-font "DejaVu Sans Mono-10")

(fset 'yes-or-no-p 'y-or-n-p)

(setq monokai-red "#DC143C")
(load-theme 'monokai t)

;; display line numbers in margin
(global-display-line-numbers-mode)

(column-number-mode 1) ;; display cursor column position

;; setting to get rid of delay when closing frame
(setq x-select-enable-clipboard-manager nil)

(electric-pair-mode 1) ;; turn on automatic bracket insertion by pairs

;; turn on paren match highlighting
(setq show-paren-delay 0)
(show-paren-mode 1)
(setq show-paren-style 'mixed)

;; make indentation commands use space only (never tab character), set default tab char's display width to 4 spaces and make tab key do indent first then completion
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
(setq-default tab-always-indent 'complete)

(require 'org-bullets)
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))

;; defined TODO task list files
(setq org-agenda-files (list "~/Nextcloud/todo/personal.org"
                             "~/Nextcloud/todo/professional.org" 
                             "~/Nextcloud/todo/open-source.org"))
	
;; defined backup and autosave directory with flat structure
(setq backup-directory-alist
          `((".*" . ,"~/.emacs.d/emacs-backup/")))
(setq backup-by-copying t)
(setq kept-new-versions 10)
(setq version-control t)
(setq delete-old-versions t)
(setq auto-save-file-name-transforms
          `((".*" ,"~/.emacs.d/emacs-autosaved/" t)))

;; disable saving local backup files  for TRAMP
(add-to-list 'backup-directory-alist
             (cons tramp-file-name-regexp nil))

(setq backup-enable-predicate
      (lambda (name)
        (and (normal-backup-enable-predicate name)
             (not
              (let ((method (file-remote-p name 'method)))
                (when (stringp method)
                  (member method '("su" "sudo" "ssh" "scp"))))))))

(setq tramp-auto-save-directory temporary-file-directory)
(setq tramp-connection-timeout 30)

(setq recentf-max-saved-items 100)
(recentf-mode 1) ;; keep a list of recently opened files

(setq create-lockfiles nil) ;; disable creating lock files

(global-visual-line-mode 1) ;; line soft wrapping at word boundary

(tool-bar-mode -1) ;; disable tool bar

(require 'awesome-tab)
(setq awesome-tab-height 115)
(awesome-tab-mode t)

(yas-global-mode 1)

;; Capitalize keywords in SQL mode
(add-hook 'sql-mode-hook 'sqlup-mode)
;; Capitalize keywords in an interactive session (e.g. psql)
(add-hook 'sql-interactive-mode-hook 'sqlup-mode)

(add-hook 'sql-mode-hook 'edbi-minor-mode)

(require 'nginx-mode)
(require 'poly-ansible)
(require 'salt-mode)

(require 'doom-modeline)
(setq doom-modeline-icon t)
(setq doom-modeline-major-mode-icon t)
(setq doom-modeline-major-mode-color-icon t)
(setq doom-modeline-buffer-state-icon t)
(setq doom-modeline-buffer-modification-icon t)
(setq doom-modeline-buffer-file-name-style 'file-name)
(setq doom-modeline-irc nil)
(doom-modeline-mode 1)

(require 'back-button)
(back-button-mode 1)

(move-text-default-bindings)

(global-flycheck-mode)
