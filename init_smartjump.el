(require 'smart-jump)
(require 'dumb-jump)

(setq dumb-jump-selector 'ivy)
(setq dumb-jump-git-grep-search-untracked nil)

(setq smart-jump-python-force-elpy t)
(setq smart-jump-bind-keys nil)
(setq smart-jump-bind-keys-for-evil nil)

(smart-jump-setup-default-registers)
