(require 'ergoemacs-mode)
(setq ergoemacs-theme nil) ;; Uses Standard Ergoemacs keyboard theme
(setq ergoemacs-keyboard-layout "us") ;; Assumes QWERTY keyboard layout
;; try to remove this because it was addressed in the ergoemacs-mode code
;;(setq ergoemacs-theme-options (quote ((save-options-on-exit off))))
(setq ergoemacs-handle-ctl-c-or-ctl-x 'only-copy-cut)
(setq ergoemacs-keep-region-after-copy t)
(setq ergoemacs-smart-paste nil)

;; override (backward)-kill-word/line to (backward)-delete-word/line to not clutter clipboard(kill-ring)
(defun delete-word (arg)
  "Delete characters forward until encountering the end of a word.
With argument, do this that many times."
  (interactive "p")
  (delete-region (point) (progn (forward-word arg) (point))))

(defun backward-delete-word (arg)
  "Delete characters backward until encountering the end of a word.
With argument, do this that many times."
  (interactive "p")
  (delete-word (- arg)))

(defun backward-delete-line (arg)
  "Delete (not kill) the current line, backwards from cursor.
With argument ARG, do this that many times."
  (interactive "p")
  (delete-region (point) (progn (beginning-of-visual-line arg) (point))))

(defun delete-line (arg)
  "Delete (not kill) the current line, forwards from cursor.
With argument ARG, do this that many times."
  (interactive "p")
  (delete-region (point) (progn (end-of-visual-line arg) (point))))

(defun delete-current-line (arg)
  "Delete (not kill) the current line."
  (interactive "p")
  (save-excursion
    (delete-region
     (progn (forward-visible-line 0) (point))
     (progn (forward-visible-line arg) (point)))))

(ergoemacs-component my-ergomods ()
  "My ergoemacs mods"
   :layout "us"
   (define-key ergoemacs-keymap (kbd "<C-delete>") 'delete-word)
   (define-key ergoemacs-keymap (kbd "<C-backspace>") 'backward-delete-word)
   (define-key ergoemacs-keymap (kbd "<M-delete>") 'delete-line)
   (define-key ergoemacs-keymap (kbd "<M-backspace>") 'backward-delete-line)
   (define-key ergoemacs-keymap (kbd "<S-delete>") 'delete-current-line)
   (define-key ergoemacs-keymap (kbd "<S-backspace>") 'delete-current-line)
   (define-key ergoemacs-keymap (kbd "M-e") 'ivy-switch-buffer)
   (define-key ergoemacs-keymap (kbd "C-e") 'counsel-recentf)
   (define-key ergoemacs-keymap (kbd "M-<left>") nil)
   (define-key ergoemacs-keymap (kbd "M-<right>") nil)
   (define-key ergoemacs-keymap (kbd "C-S-f") nil)
   (define-key ergoemacs-keymap (kbd "C-/") nil)
   (define-key ergoemacs-keymap (kbd "M-z") nil)
   (define-key ergoemacs-keymap (kbd "M-Z") nil)
   (define-key ergoemacs-keymap (kbd "M-x") nil)
   (define-key ergoemacs-keymap (kbd "M-X") nil)
   (define-key ergoemacs-keymap (kbd "M-c") nil)
   (define-key ergoemacs-keymap (kbd "M-C") nil)
   (define-key ergoemacs-keymap (kbd "M-d") nil)
   (define-key ergoemacs-keymap (kbd "M-e") nil)
   (define-key ergoemacs-keymap (kbd "M-r") nil)
   (define-key ergoemacs-keymap (kbd "M-t") nil)
   (define-key ergoemacs-keymap (kbd "C-b") nil))

(ergoemacs-require 'swiper)
(ergoemacs-require 'my-ergomods)

;; this is because when I've installed pcache and persistent-soft packages things fucked up after restart like C-f, C-c and alike
(ergoemacs-mode-clear-cache)
(ergoemacs-mode 1)
