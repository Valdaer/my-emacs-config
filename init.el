package-initialize)

(load "~/.emacs.d/init_ivy")
(load "~/.emacs.d/init_company")
(load "~/.emacs.d/init_elpy")
(load "~/.emacs.d/init_smartjump")
(load "~/.emacs.d/init_misc")
(load "~/.emacs.d/init_keybinding")
(load "~/.emacs.d/init_ergoemacs")
